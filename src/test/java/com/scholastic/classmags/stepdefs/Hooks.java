package com.scholastic.classmags.stepdefs;

import static com.scholastic.torque.common.TestBaseProvider.getTestBase;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.scholastic.torque.common.ScreenshotUtil;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;
import com.torque.automation.core.TestDataUtils;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

@SuppressWarnings("unused")
public class Hooks {
	String status = null;
	static {
		System.setProperty("webdriver.gecko.driver", "servers/geckodriver.exe");
		System.setProperty("webdriver.firefox.marionette", "servers/geckodriver.exe");
		System.setProperty("webdriver.ie.driver", "servers/IEDriverServer.exe");
	}

	@Before
	public void beforeHook(Scenario scenario) {
		synchronized (this) {
			TestBase testBase = TestBaseProvider.getTestBase();
			getTestBase().getContext().setProperty("ScenarioName", scenario.getName());
			
			// //Below one line is dashboard related
			// TestDataUtils.getAndStorePlatformDetails(System.getProperty("outputdir"));
			// Calling populateTestdata method from Torque2.0
			TestDataUtils.populateTestdata(scenario.getName());
			testBase.getContext().subset("testexecution").clear();
			String session = testBase.getSessionID();
			System.out.println("session:::::::::::::::::::"+ session);
			if (!session.equalsIgnoreCase("") && !testBase.getContext().getString("sauce").equalsIgnoreCase("false")) {
				String sname = scenario.getName();
				((JavascriptExecutor) testBase.getDriver()).executeScript("lambda-name=" + sname);
			}
			try {
				testBase.getDriver().manage().deleteAllCookies();
			} catch (Exception e) {
				status = "Skipped";
			}
			testBase.getDriver().get(testBase.getString("url"));
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@After
	public void afterHook(Scenario scenario) throws URISyntaxException, ClientProtocolException, IOException {
		synchronized (this) {

			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			if (scenario.isFailed()) {
				System.out.println("Method Failed::::::::::::" + TestBaseProvider.getTestBase().getString("Method"));
				try {
					scenario.write("Current Page URL is " + driver.getCurrentUrl());
				} catch (WebDriverException somePlatformsDontSupportScreenshots) {
					System.err.println(somePlatformsDontSupportScreenshots.getMessage());
				}
			}

			String session = TestBaseProvider.getTestBase().getSessionID();
			TestBase testBase = TestBaseProvider.getTestBase();

			String status = null;
			if (testBase.getContext().getString("lambdaTest").equalsIgnoreCase("true")) {
				if (!scenario.isFailed()) {
					status = "passed";
					((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
					String sname = scenario.getName();
					((JavascriptExecutor) testBase.getDriver()).executeScript("lambda-name=" + sname);
				} else {
					status = "failed";
					((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
					String sname = scenario.getName();
					((JavascriptExecutor) testBase.getDriver()).executeScript("lambda-name=" + sname);
				}
			}

			if (session.isEmpty() || session == null) {
				session = "NoSession/TC is Skipped";
			}
			System.out.println("Session id is: " + session);
			System.out.println("ScenarioFailed=" + scenario.getName() + "<>Session=" + session + "<>Status=" + status
					+ "<>Platform=" + testBase.getString("driver.name"));
			System.out.println("SauceOnDemandSessionID=" + session + " job-name=" + scenario.getName());

			status = null;
			ScreenshotUtil.takeScreenshot(scenario);
			TestBaseProvider.getTestBase().tearDown();
		}
	}
}